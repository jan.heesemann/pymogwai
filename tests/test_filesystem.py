import unittest
from mogwai.filesystem import FileSystemGraph as FSG
from mogwai.graph import Node
import os

class TestFileSystem(unittest.TestCase):
    def setUp(self):
        # Initialize a sample file system structure for testing
        self.sample_root_path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        self.file_system = FSG(self.sample_root_path)

    def test_readme(self):
        readme_nodes = self.file_system.get_nodes("README.md")
        self.assertTrue(len(readme_nodes)==1, "Incorrect number of readme's")

        readme_node:Node = readme_nodes[0]
        properties = readme_node.properties
        self.assertTrue("type" in properties and properties["type"]=="file", "Incorrect or missing type for README.md")
        self.assertTrue("filesize" in properties, "No filesize property")
        self.assertTrue(properties["filesize"]==6102, "Filesize is not correct")

if __name__ == "__main__":
    unittest.main()
