from .graph import Graph, Node
import os

class FileSystemGraph(Graph):
    def __init__(self, root_path):
        super().__init__()
        self.root_path = root_path
        self._extract_graph_structure()

    def _get_file_stats(self, file) -> dict:
        res = os.stat(file)
        stats = {"filesize": res.st_size}
        return stats

    def _extract_graph_structure(self):
        if not os.path.exists(self.root_path): raise ValueError(f"Path {self.root_path} does not exist.")
        if os.path.isfile(self.root_path):
            self.add_node(Node(self.root_path, type='file'))
            return
        
        stack = [(self.root_path, None)]
        while stack:
            current_folder, parent_node = stack.pop()
            current = self.add_node(Node(os.path.basename(current_folder), type='directory'))
            if(parent_node): self.add_edge(parent_node, current, "has_child")

            for item in os.listdir(current_folder):
                abspath = os.path.abspath(os.path.join(current_folder, item))
                if os.path.isfile(abspath):
                    node = self.add_node(Node(item, properties=self._get_file_stats(abspath), type='file'))
                    self.add_edge(current, node, "has_child")
                else:
                    stack.append((abspath, current))