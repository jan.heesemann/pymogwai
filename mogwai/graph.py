from typing import List

class Edge:
    def __init__(self, a, b, relation):
        self.a = a
        self.b = b
        self.relation=relation

class Node:
    def __init__(self, name:str, properties:dict={},**kwargs):
        self.name = name
        self.properties = properties
        self.properties.update(kwargs)
        self.in_edges = []
        self.out_edges = []

    def has_property(self, key):
        return key in self.properties

    def get_property(self, key):
        return self.properties[key]

class Graph:
    def __init__(self, nodes:List[Node]=[], edges:List[Edge]=[]):
        self.nodes = nodes
        self.edges = edges
    
    def get_nodes(self, name:str) -> Node:
        return list(filter(lambda n: n.name==name, self.nodes))            

    def add_node(self, node:Node):
        self.nodes.append(node)
        return node
    
    def add_edge(self, node_a:Node, node_b:Node, relation):
        #we should check that a and b are part of the graph
        edge = Edge(node_a, node_b, relation)
        node_a.out_edges.append(edge)
        node_b.in_edges.append(edge)
        self.edges.append(edge)
        return edge